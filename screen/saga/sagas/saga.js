import {all, put, takeEvery} from 'redux-saga/effects';


export function* sayinc() {
    console.log('INC');
    setTimeout(() => {
        alert('inc');
    }, 5000);
}

export function* saydec(){
    console.log('DEC');
    setTimeout(() => {    
        alert('dec');
    }, 5000);
}

export function* takeInc() {
    yield takeEvery('UP', sayinc);
}

export function* takeDec() {
    yield takeEvery('DOWN', saydec);
}

export default function* rootSaga(){
    yield all([
        takeInc(),
        takeDec(),
    ]);
}
